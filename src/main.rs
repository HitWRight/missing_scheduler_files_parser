mod missing_file;

use aurora_client::client::common::CommonCalls;
use aurora_client::client::screen::ScreenCalls;
use aurora_client::client::Client;
use aurora_client::models::screen::ScreenLogin;
use aurora_client::models::subscriptions::subscriber::Subscriber;
use aurora_client::models::user::User;
use clap::Parser;
use itertools::Itertools;
use lettre::message::SinglePart;
use lettre::Transport;
use log::{debug, info, warn};
use missing_file::MissingFile;
use std::collections::BTreeMap;
use std::fmt::Write;
use std::io::ErrorKind;

/// Scans the folders in Working Directory and searches for MissingFiles.log files.
/// Parses the log files and joins the results with Aurora users data.
/// After data aggregation, sends out an email to subscribed Aurora users.
/// Works only with SSL authentication to SMTP server.
#[derive(Parser)]
#[clap(author, version, about)]
struct Opts {
    /// Sets the email (SMTP) server to connect to
    #[clap(short('s'), long)]
    email_server: String,
    /// SMTP username
    #[clap(short('u'), long)]
    email_user: String,
    /// SMTP password
    #[clap(short('p'), long)]
    email_pass: String,
    /// Aurora rest api hostname
    #[clap(short, long)]
    rest_hostname: String,
    /// Aurora rest api app id
    #[clap(long)]
    rest_app_id: String,
    /// Aurora rest api key
    #[clap(long)]
    rest_key: String,
    /// Aurora rest api user impersonation id
    #[clap(long)]
    rest_user_id: i32,
}

#[tokio::main]
async fn main() {
    env_logger::init();
    let opts = Opts::parse();
    //get companies
    let rest_client = Client::new(
        &opts.rest_app_id,
        &opts.rest_key,
        &opts.rest_hostname,
        opts.rest_user_id,
    );
    let companies = CommonCalls::get_companies(&rest_client).await.unwrap();
    let screens = ScreenCalls::get_screen_logins(&rest_client).await.unwrap();
    //get missing_files after getting screens
    let (missing_files, failed_screens) = retrieve_missing_files(screens).await;
    //get subscribed users/companies list
    let subscribed_users = rest_client.get_subscriptions_missing_files_subscribers();
    //get users list to inform for each entry (get by video number)
    let missing_files = fill_out_users_to_inform(&rest_client, missing_files).await;
    // println!("{:?}", missing_files);
    let subscribed_users = subscribed_users.await;
    //send to subscribed users
    for company in companies {
        //filter missing_files by company
        let subscribed_users = subscribed_users
            .clone()
            .into_iter()
            .filter(|su| su.company_id == company.id)
            .collect::<Vec<Subscriber>>();
        let missing_files = missing_files
            .clone()
            .into_iter()
            .filter(|mf| {
                mf.0.iter()
                    .filter(|user| user.company_id == company.id)
                    .count()
                    > 0
            })
            .collect::<BTreeMap<Vec<User>, Vec<MissingFile>>>();

        send_email(
            &opts,
            &company.name,
            subscribed_users,
            missing_files,
            failed_screens.clone(),
        )
        .await;
    }
}

async fn send_email(
    opts: &Opts,
    company_name: &str,
    users: Vec<Subscriber>,
    missing_files: BTreeMap<Vec<User>, Vec<MissingFile>>,
    failed_screens: Vec<ScreenLogin>,
) {
    let body = match missing_files.len() {
        0 => std::fs::read_to_string("assets/allfound.html")
            .expect("assets/allfound.html was not found"),
        _ => std::fs::read_to_string("assets/template.html")
            .expect("assets/allfound.html was not found"),
    };

    let body = body.replace(
        "{FailedScreens}",
        &failed_screens
            .iter()
            .map(|screen| screen.name.clone())
            .collect::<Vec<String>>()
            .join(", "),
    );

    let body = body.replace("{MissingFiles}", &(format_missing_file(missing_files)));

    for subscriber in users {
        let email = lettre::Message::builder()
            .from(
                format!("Ponas Pradinguolis <{}>", opts.email_user)
                    .parse()
                    .unwrap(),
            )
            .to(subscriber.email.parse().unwrap())
            .subject(&format!("Trūkstami klipai: {}", company_name))
            .singlepart(SinglePart::html(body.clone()))
            .unwrap();

        let sender = lettre::SmtpTransport::starttls_relay(&opts.email_server)
            .unwrap()
            .credentials(lettre::transport::smtp::authentication::Credentials::new(
                opts.email_user.clone(),
                opts.email_pass.clone(),
            ))
            .authentication(vec![
                lettre::transport::smtp::authentication::Mechanism::Login,
            ])
            .pool_config(lettre::transport::smtp::PoolConfig::new().max_size(20))
            .build();

        // Send the email
        match sender.send(&email) {
            Ok(_) => println!("Email sent successfully!"),
            Err(e) => panic!("Could not send email: {:?}", e),
        }
    }
}

fn format_missing_file(missing_files: BTreeMap<Vec<User>, Vec<MissingFile>>) -> String {
    let mut result = String::new();
    for (key, mut group) in missing_files {
        group.sort_by(|a, b| match a.date > b.date {
            true => std::cmp::Ordering::Greater,
            false => std::cmp::Ordering::Less,
        });

        write!(
            &mut result,
            "<b>{}</b><br>",
            key.iter()
                .map(|user| format!("{} {}", user.first_name, user.last_name))
                .join(", ")
        )
        .unwrap();
        result.push_str("---------------------------------------------<br>");
        result.push('\n');

        for elem in group {
            write!(&mut result,
                "\"{}\" <i>Nr</i>: {} <i>Startas:</i> {}<br><i>Ekranai:</i> {}<br><br>",
                elem.client_name,
                elem.video_number,
                elem.date,
                elem.screens
                    .into_iter()
                    .map(|scr| scr.name)
                    .collect::<Vec<String>>()
                    .join(", "),
            ).unwrap()
        }
    }

    result
}

async fn retrieve_missing_files(screens: Vec<ScreenLogin>) -> (Vec<MissingFile>, Vec<ScreenLogin>) {
    let mut result: Vec<MissingFile> = vec![];
    let mut failed: Vec<ScreenLogin> = vec![];
    info!("Scanning directories for log files");
    for screen in screens {
        let filepath = format!("{}/MissingFiles.log", screen.name.replace(' ', "_"));
        debug!("Searching for {}", filepath);
        match tokio::fs::read_to_string(filepath).await {
            Ok(data) => {
                let parsed_data = MissingFile::parse(data);

                for new_missing_file in parsed_data.iter() {
                    match result.iter_mut().find(|mf| *mf == new_missing_file) {
                        Some(found) => found.screens.push(screen.clone()),
                        None => {
                            let mut new_missing_file = new_missing_file.clone();
                            new_missing_file.screens.push(screen.clone());
                            result.push(new_missing_file)
                        }
                    }
                }
            }
            Err(err) if err.kind() == ErrorKind::NotFound => {
                warn!("Screen ({}) log file not found.", screen.name);
                failed.push(screen);
            }
            Err(err) => {
                panic!("Error path not handled: {:?}", err);
            }
        }
    }
    (result, failed)
}

async fn fill_out_users_to_inform(
    rest_client: &Client,
    missing_files: Vec<MissingFile>,
) -> BTreeMap<Vec<User>, Vec<MissingFile>> {
    info!("Retrieving users data for missing files");
    let mut futures = vec![];

    for missing_file in missing_files.into_iter() {
        futures.push(fill_out_user_to_inform(rest_client, missing_file));
    }

    let mut missing_files = vec![];
    for future in futures {
        missing_files.push(future.await);
    }

    let mut result: BTreeMap<Vec<User>, Vec<MissingFile>> = BTreeMap::new();
    for missing_file in missing_files {
        let mut users: Vec<_> = missing_file.1.into_iter().unique().collect();
        users.sort_by(|a, b| {
            a.last_name
                .cmp(&b.last_name)
                .then(a.first_name.cmp(&b.first_name))
        });

        match result.get_mut(&users) {
            None => {
                result.insert(users, vec![missing_file.0]);
            }
            Some(val) => val.push(missing_file.0),
        }
    }

    result
}

async fn fill_out_user_to_inform(
    rest_client: &Client,
    missing_file: MissingFile,
) -> (MissingFile, Vec<User>) {
    let result = rest_client
        .get_subscriptions_missing_files_authorized_users_by_video_number(
            &missing_file.video_number,
        )
        .await;

    (missing_file, result)
}
