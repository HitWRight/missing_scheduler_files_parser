use aurora_client::models::screen::ScreenLogin;
use chrono::NaiveDate;
use regex::Regex;

#[derive(Clone, Debug)]
pub struct MissingFile {
    pub date: NaiveDate,
    pub video_number: String,
    pub screens: Vec<ScreenLogin>,
    pub client_name: String,
}

impl MissingFile {
    pub fn parse(data: String) -> Vec<MissingFile> {
        data.split("\r\n")
            .filter_map(|line| {
                let re = Regex::new(r"(.*) is missing: (.*) first time playing: (.*)").unwrap();
                match re.captures(line) {
                    Some(cap) => Some(MissingFile {
                        client_name: cap.get(1).map_or("", |m| m.as_str()).to_string(),
                        video_number: cap.get(2).map_or("", |m| m.as_str()).to_string(),
                        date: NaiveDate::parse_from_str(
                            cap.get(3).map_or("0001-01-01", |m| m.as_str()),
                            "%Y-%m-%d",
                        )
                        .unwrap(),
                        screens: vec![],
                    }),
                    None => None,
                }
            })
            .collect::<Vec<MissingFile>>()
    }
}

impl PartialEq for MissingFile {
    fn eq(&self, other: &Self) -> bool {
        self.client_name == other.client_name && self.video_number == other.video_number
    }
}
