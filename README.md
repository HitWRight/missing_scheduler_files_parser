

# Aprašymas

Rust pasiremta technologija.  
Surenka Missing file log'us iš ekranų į bendrą vietą.  
Sumatchina trukstamų klipų užsakyums su duomenų baze.  
Sugrupuoja duomenis patogiam atvaizdavimui.  
Išsiunčia email'us visiems užregistruotiems varotojams, kam tai yra aktualu.  


# Reikalavimai

-   MSSQL 2017 Server ODBC driver.
-   aurora\_api\_common biblioteka.


# Kompiliavimas

-   Susidiegiami Rust compiliavimo įrankiai (Rustup)
-   paleidžiama "cargo build komanda"


# Instaliavimas

-   Nukopijuojamas .exe failas
-   Sekamos programos instrukcijos konfiguruojant connection
-   Pakuriamas task'as po task scheduler periodiškumui


# Testavimas

Nėra (Numatyta naudot cargo test)  

